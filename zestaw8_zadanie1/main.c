#include <stdio.h>
#include <stdlib.h>
#include "glowa1.h"

void Test(FILE *file, float *x, float *y, float *rho);
int main()
{
    FILE *file;
    file=fopen("P0001_attr.txt","r+w");

    if(file == NULL){
        printf("null");
        exit(1);
    }
    else
    {
        float *x;
        x = (float *)calloc(50, sizeof(float));
        float *y ;
        y = (float *)calloc(50, sizeof(float));
        float *rho;
        rho= (float *)calloc(50, sizeof(float));

         Test(file, x, y, rho);

         printf("\n\nDane dla kolumny X");

         fprintf(file, "\nSREDNIA\t\tMEDIANA\t\tODCH.ST\n");

         float mean_x = Mean(x, 50);
         float median_x = Mediana(x, 50);
         float sd_x = SD(x,  50);

         fprintf(file, "%f\t%f\t%f\n", mean_x, median_x, sd_x);

         printf("\n\nDane dla kolumny Y");
         float mean_y = Mean(y,  50);
         float median_y = Mediana(y, 50);
         float sd_y = SD(y, 50);

         fprintf(file, "%f\t%f\t%f\n", mean_y, median_y, sd_y);

         printf("\n\nDane dla kolumny RHO");
         float mean_rho = Mean(rho, 50);
         float median_rho = Mediana(rho, 50);
         float sd_rho = SD(rho,  50);

         fprintf(file, "%f\t%f\t%f\n", mean_rho, median_rho, sd_rho);

    free(x);
    free(y);
    free(rho);

    fclose(file);

    }

    return 0;
}

void Test(FILE *file, float *x, float *y, float *rho){
    fseek(file, 16, SEEK_SET);

    for(int i=0; i<9; i++)
    {
        fscanf(file, "%f", &x[i]);
        printf("%.5f\t",x[i]);

        fseek(file, 2,  SEEK_CUR);
        fscanf(file,"%f", &y[i]);
        printf("%.5f\t",y[i]);

        fseek(file, 1,SEEK_CUR);
        fscanf(file, "%f", &rho[i]);
        printf("%.3f\n",rho[i]);

        fseek(file, 4, SEEK_CUR);
    }

    fseek(file, 1, SEEK_CUR);

    for(int i=9; i<50; i++)
    {
        fscanf(file, "%f", &x[i]);
        printf("%.5f\t", x[i]);

        fseek(file, 2, SEEK_CUR);
        fscanf(file,"%f", &y[i]);
        printf("%.5f\t",y[i]);

        fseek(file, 1,SEEK_CUR);
        fscanf(file, "%f", &rho[i]);
        printf("%.3f\n",rho[i]);

        fseek(file, 5, SEEK_CUR);
    }
    return;
}