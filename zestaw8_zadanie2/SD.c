//
// Created by Monika on 2020-05-09.
//

#include <stdio.h>
#include <math.h>
#include "glowa.h"

double SD(double *data, int size) {
    double sum = 0.0;
    double mean = 0.0;
    double SD = 0.0;

    int i;
    for (i = 0; i < size; ++i) {
        sum += data[i];
    }
    mean = sum / size;
    for (i = 0; i < size; ++i)
        SD += pow(data[i] - mean, 2);
    SD=sqrt(SD / size);
    printf("\nODCHYLENIE STANDARDOWE= %lf", SD);

    return SD;
}



