//
// Created by Monika on 2020-05-09.
//
#include <stdio.h>
#include "glowa.h"

double Mediana(double *x, int size) {
    double temp=0.0;

    for (int i = 0; i < 50; i++) {
        for (int j = 0; j < 50 - i; j++) {
            if (x[j - 1] > x[j]) {
                temp=x[j] ;
                x[j] = x[j - 1];
                x[j - 1] = temp;
            }
        }
    }

    int half= size/2;
    double median = (x[half]+x[half-1])/2;
    printf("\nMEDIANA = %lf", median);
    return median;
}
